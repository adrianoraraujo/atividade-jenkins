package domain;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

public class FatorialTest
{
	

	@Test
	public void testarFatorial_6() {
		// Executar o mtodo que ser testado e obter o resultado atual
		Integer fatorialAtual = Fatorial.calcularFatorial(6);
		Integer fatorialEsperado = 720;

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
	

}
